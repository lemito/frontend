/** @jsx jsx */

import React from "react";
import { css, jsx } from '@emotion/react';
import styled from '@emotion/styled'
import Button from '@mui/material/Button';

const TableContent = styled.div`
height: "auto", 
textAlign: "center",
fontFamily: "Verdana",
lineHeight: 5
`

function About(props) {

    function handleClick() {
        alert('Ты создал ошибку. Ее можно посмотреть в console. А еще, она уже отправлена на Sentry и скоро будет пофикшена')
        throw new Error('Uncaught');
    }

    return (
    <div>    
        <TableContent className="table-content" css={css`
        height: "auto", 
        textAlign: "center",
        fontFamily: "Verdana",
        lineHeight: 5`}>
                <label>This is an About page. (under construction)</label>
                <br/>
                <label>Created with Spring Boot and React</label>
                <br/>
                <label>Create by JW Studio</label>
            <br/>
                <Button onClick={handleClick}>Break the world</Button>
        </TableContent>
    </div>
    )

}

export default About;