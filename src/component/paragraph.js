import {useParams} from "react-router";
import React from "react";

function Paragraph(props) {
    const paragraphs = props.content.replace(/(\r)/g, "").split("\n");
    const {id} = useParams();
    return (
        <div style={{width: "95%", margin: "auto"}} className={props.className || "readonly-field-textarea"}>
            <div className={"content-of-article-with-id_"+id}>{paragraphs.map((el, index) => <div key={index} id={index+' '+'text-content'} dangerouslySetInnerHTML={{__html: el}}/>, )}</div>
        </div>
    );
}

export default Paragraph;