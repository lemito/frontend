import React from "react";
import ReactDOM from "react-dom";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import App from "./app";
//require("@babel/polyfill");

/*function App() {

}*/
Sentry.init({
    dsn: "https://ba475e16810643d684ff91430fb1c28c@o339714.ingest.sentry.io/6220255",
    tracesSampleRate: 1.0,

});

ReactDOM.render(<App/>, document.getElementById("root"));