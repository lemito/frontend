Use https://start.spring.io/ to setup

Add dependencies: Spring Boot DevTools, Lombok, Spring Web, MyBatis Framework, Spring Security, MySQL Driver, Spring Cache Abstraction

MySQL JDBC version: 8.0.15

Lombok: build Entity using @Data

To start sever, type in Terminal "mvn spring-boot:run"

When running Server, after Edit html file, type in ANOTHER Terminal "mvn compile"

To test API, use PostMan or http://localhost:8080/swagger-ui.html

dependencies {
compile "org.springframework.boot:spring-boot-starter-thymeleaf:*"
compile "org.hibernate:hibernate-validator:6.1.5.Final"
compile "io.springfox:springfox-swagger-ui:2.9.2"
compile "io.springfox:springfox-swagger2:2.9.2"
compile "javax.cache:cache-api:*"
compile "org.ehcache:ehcache:*"
compile "com.github.pagehelper:pagehelper-spring-boot-starter:1.2.13"
compile "com.auth0:java-jwt:3.10.3"
compile "javax.xml.bind:jaxb-api:2.3.1"
compile "com.sun.xml.bind:jaxb-impl:3.0.0-M3"
compile "com.sun.xml.bind:jaxb-core:3.0.0-M3"
compile "javax.activation:activation:1.1.1"
compile "org.springframework.boot:spring-boot-starter-cache:*"
compile "org.springframework.boot:spring-boot-starter-security:*"
compile "org.springframework.boot:spring-boot-starter-web:*"
compile "org.mybatis.spring.boot:mybatis-spring-boot-starter:2.1.2"
compile "org.springframework.boot:spring-boot-devtools:*"
compile "org.postgresql:postgresql:*"
compile "org.projectlombok:lombok:*"
testCompile "org.springframework.boot:spring-boot-starter-test:*"
testCompile "org.springframework.security:spring-security-test:*"
compile "org.springframework.security:spring-security-core:[5.3.2,)"
compile "javax.persistence:javax.persistence-api:2.2"
}
